import { McfAccordionModule } from '@accelya/sdk/mcf-accordion';
import { NgModule } from '@angular/core'
import { McfHeaderModule } from '@accelya/sdk/mcf-header';
import { McfFooterModule } from '@accelya/sdk/mcf-footer'
import { McfInputModule } from "@accelya/sdk/mcf-input";
import { McfButtonModule } from "@accelya/sdk/mcf-button";
import { McfModalModule } from '@accelya/sdk/mcf-modal';

@NgModule({
    declarations: [

    ],
    imports: [
        McfAccordionModule,
        McfHeaderModule,
        McfFooterModule,
        McfInputModule,
        McfButtonModule,
        McfModalModule
    ],
    exports: [
        McfAccordionModule,
        McfHeaderModule,
        McfFooterModule,
        McfInputModule,
        McfButtonModule,
        McfModalModule
    ]
  })
  export class SharedModule { }